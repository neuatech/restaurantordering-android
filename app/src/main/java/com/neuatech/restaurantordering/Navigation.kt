package com.neuatech.restaurantordering

/**
 * Handles the navigation in [SingleActivity]
 */
interface Navigation {
    /**
     * Shows the [LoginFragment].
     */
    fun onLogin()

    /**
     * Shows the [RestaurantSearchFragment].
     */
    fun onSearchRestaurant()
}