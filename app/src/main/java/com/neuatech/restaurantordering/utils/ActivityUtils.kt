package com.neuatech.restaurantordering.utils

import android.view.View
import androidx.fragment.app.Fragment
import com.neuatech.restaurantordering.R
import com.neuatech.restaurantordering.SingleActivity
import kotlinx.android.synthetic.main.activity_single.*

fun SingleActivity.pushFragment(fragment: Fragment) {
    supportFragmentManager
        .beginTransaction()
        .replace(
            R.id.fragment_container, fragment)
        //.setCustomAnimations()
        .commitAllowingStateLoss()
}

fun SingleActivity.startUpToolbar() {
    toolbar.visibility = View.GONE
    fragment_container.setPadding(0, 0, 0, 0)
}