package com.neuatech.restaurantordering.auth

import android.content.Context
import com.facebook.AccessToken
import com.facebook.login.LoginManager

object AuthenticationManager {

    object Constants {
        const val PREFS_NAME = "restaurantOrderingLocalStorage"
        const val PRIVATE_MODE = 0
        const val JWT_TOKEN = "jwtToken"
    }

    /**
     * Checks if there is a user logged in.
     */
    fun isLoggedIn(context: Context): Boolean {
        val token = AccessToken.getCurrentAccessToken()
        if (token == null || token.isExpired) {
            LoginManager.getInstance().logOut()
            removeToken(context)
            return false
        }
        if (getToken(context).isEmpty()) {
            return false
        }
        return true
    }

    /**
     * Stores the auth token in private storage.
     */
    fun storeToken(context: Context, jwtToken: String) {
        context.getSharedPreferences(Constants.PREFS_NAME, Constants.PRIVATE_MODE).edit().apply {
            putString(Constants.JWT_TOKEN, jwtToken)
            apply()
        }
    }

    fun getToken(context: Context): String =
        context.getSharedPreferences(Constants.PREFS_NAME, Constants.PRIVATE_MODE).getString(
            Constants.JWT_TOKEN,
            ""
        )!!

    /**
     * Removes the auth token from the private storage.
     */
    fun removeToken(context: Context) {
        context.getSharedPreferences(Constants.PREFS_NAME, Constants.PRIVATE_MODE).edit().apply {
            remove(Constants.JWT_TOKEN)
            apply()
        }
    }
}