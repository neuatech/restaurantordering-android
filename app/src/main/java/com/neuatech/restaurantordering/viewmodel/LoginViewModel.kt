package com.neuatech.restaurantordering.viewmodel

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.neuatech.restaurantordering.api.AuthApi
import com.neuatech.restaurantordering.api.model.FacebookUser
import com.neuatech.restaurantordering.common.Outcome
import com.neuatech.restaurantordering.fragment.LoginFragment.Companion.FACEBOOK_EMAIL_PERMISSION
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class LoginViewModel(callbackManager: CallbackManager, private val authApi: AuthApi) : ViewModel() {

    private val _outcome = MutableLiveData<Outcome<String>>()

    val outcome: LiveData<Outcome<String>>
        get() = _outcome

    private val facebookCallback : FacebookCallback<LoginResult> by lazy {
        object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {

                GraphRequest.newMeRequest(
                    result!!.accessToken
                ) { json, response ->

                    if (response.error != null) {
                        _outcome.postValue(Outcome.Failure(response.error!!.errorMessage))
                        return@newMeRequest
                    }

                    val email = json.optString(FACEBOOK_EMAIL_PERMISSION)
                    val profile = Profile.getCurrentProfile()

                    if (profile != null) {
                        val facebookUser = FacebookUser(
                            result.accessToken.token,
                            profile.firstName,
                            profile.lastName,
                            email
                        )

                        continueWithJwtToken(facebookUser)
                        return@newMeRequest
                    }

                    object : ProfileTracker() {
                        override fun onCurrentProfileChanged(oldProfile: Profile?, currentProfile: Profile?) {
                            val facebookUser = FacebookUser(
                                result.accessToken.token,
                                currentProfile!!.firstName,
                                currentProfile.lastName,
                                email
                            )

                            continueWithJwtToken(facebookUser)
                        }
                    }
                }.apply {
                    val parameters = Bundle()
                    parameters.putString("fields", "name,email")
                    setParameters(parameters)
                }.executeAsync()
            }

            override fun onCancel() {
                _outcome.postValue(Outcome.Failure("User canceled the login process."))
            }

            override fun onError(error: FacebookException?) {
                _outcome.postValue(Outcome.Failure(error!!.message!!))
            }
        }
    }

    init {
        LoginManager.getInstance().registerCallback(callbackManager, facebookCallback)
    }

    /**
     * Starts the facebook login.
     */
    fun login(fragment: Fragment) {
        LoginManager.getInstance().logInWithReadPermissions(fragment, listOf(FACEBOOK_EMAIL_PERMISSION))

        _outcome.postValue(Outcome.Loading)
    }

    private fun continueWithJwtToken(facebookUser: FacebookUser) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val response = authApi.getJWTTokenAsync(facebookUser).await()
                    _outcome.postValue(Outcome.Success(response.payload as String))
                } catch (e: Exception) {
                    _outcome.postValue(Outcome.Failure(e.message!!))
                }
            }
        }
    }
}