package com.neuatech.restaurantordering.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.neuatech.restaurantordering.api.RestaurantApi
import com.neuatech.restaurantordering.api.model.RestaurantModel
import com.neuatech.restaurantordering.auth.AuthenticationManager
import com.neuatech.restaurantordering.location.LocationManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class FindRestaurantViewModel(
    private val context: Context,
    private val api: RestaurantApi
) : ViewModel() {

    enum class ViewState {
        LOADING,
        LOCATION_DISABLED,
        CURRENT_LOCATION,
        NO_LOCATION,
        SUGGESTIONS
    }

    private val locationManager = LocationManager(context)

    private val _state = MutableLiveData<ViewState>()
    private val _currentLocation = MutableLiveData<RestaurantModel>()
    private val _suggestions = MutableLiveData<ArrayList<RestaurantModel>>()

    val state: LiveData<ViewState>
        get() = _state
    val currentLocation: LiveData<RestaurantModel>
        get() = _currentLocation
    val suggestions: LiveData<ArrayList<RestaurantModel>>
        get() = _suggestions

    fun findLocation() {
        _state.postValue(ViewState.LOADING)
        viewModelScope.launch {
            withContext(Dispatchers.Main) {
                if (locationManager.isLocationEnabled()) {
                    try {
                        val location = locationManager.getLocation()

                        val response = api.getNearbyRestaurantsAsync(
                            "Bearer ${AuthenticationManager.getToken(context)}",
                            location.latitude,
                            location.longitude
                        ).await()

                        val restaurants = response.payload as ArrayList<RestaurantModel>

                        if (restaurants.isEmpty()) {
                            _state.postValue(ViewState.NO_LOCATION)
                            return@withContext
                        }

                        _currentLocation.postValue(restaurants[0])
                        _suggestions.postValue(
                            arrayListOf(
                                restaurants[0],
                                restaurants[1],
                                restaurants[2]
                            )
                        )
                        _state.postValue(ViewState.CURRENT_LOCATION)
                    } catch (e: Exception) {
                        Log.d("Error message:", e.message!!)
                    }
                } else {
                    _state.postValue(ViewState.LOCATION_DISABLED)
                }
            }
        }
    }

    fun showSuggestions() {
        _state.postValue(ViewState.SUGGESTIONS)
    }
}