package com.neuatech.restaurantordering.fragment.findrestaurant

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.neuatech.restaurantordering.R
import com.neuatech.restaurantordering.api.RestaurantApi
import com.neuatech.restaurantordering.api.model.RestaurantModel
import com.neuatech.restaurantordering.common.ApiProvider
import com.neuatech.restaurantordering.common.ViewModelFactory
import com.neuatech.restaurantordering.viewmodel.FindRestaurantViewModel
import kotlinx.android.synthetic.main.fragment_restaurant_search.*

class FindRestaurantFragment : Fragment() {

    companion object {
        fun create(): FindRestaurantFragment {
            return FindRestaurantFragment()
        }
    }

    private lateinit var viewModel: FindRestaurantViewModel

    private lateinit var layouts: ArrayList<ConstraintLayout>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val api = ApiProvider.getApi(RestaurantApi::class.java) as RestaurantApi

        viewModel = ViewModelProviders.of(this,
            ViewModelFactory { FindRestaurantViewModel(context!!, api) })
            .get(FindRestaurantViewModel::class.java)

        layouts = arrayListOf(
            searchingLayout,
            locationDisabledLayout,
            singleLocationLayout,
            noLocationLayout,
            suggestionsLayout
        )

        viewModel.state.observe(this, Observer {
            when(it) {
                FindRestaurantViewModel.ViewState.LOADING -> showLayout(searchingLayout)
                FindRestaurantViewModel.ViewState.CURRENT_LOCATION -> showLayout(singleLocationLayout)
                FindRestaurantViewModel.ViewState.LOCATION_DISABLED -> showLayout(locationDisabledLayout)
                FindRestaurantViewModel.ViewState.SUGGESTIONS -> showLayout(suggestionsLayout)
                FindRestaurantViewModel.ViewState.NO_LOCATION -> showLayout(noLocationLayout)
                null -> {}
            }
        })

        viewModel.currentLocation.observe(this, Observer {
            populateCurrentLocation(it)
        })

        viewModel.suggestions.observe(this, Observer {
            populateSuggestions(it)
        })

        tryAgainButton.setOnClickListener { viewModel.findLocation() }
        tryAgainButtonNoLocation.setOnClickListener { viewModel.findLocation() }
        notHereTextView.setOnClickListener { viewModel.showSuggestions() }
        settingsButton.setOnClickListener { startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)) }

        viewModel.findLocation()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_restaurant_search, container, false)
    }

    private fun showLayout(layout: ConstraintLayout) {
        val shownLayout = layouts.find { it.visibility == View.VISIBLE }
        if (shownLayout != null && shownLayout.id == layout.id) {
            return
        }

        if (shownLayout != null) {
            shownLayout.isEnabled = false
            shownLayout.visibility = View.GONE
        }
        layout.isEnabled = true
        layout.visibility = View.VISIBLE
    }

    private fun populateCurrentLocation(restaurant: RestaurantModel) {
        locationTextView.text = restaurant.name
    }

    private fun populateSuggestions(restaurants: ArrayList<RestaurantModel>) {
        val adapter = SuggestionsAdapter(context!!, restaurants,
            object : SuggestionsAdapter.OnSuggestionClicked{
                override fun onClick(restaurant: RestaurantModel) {
                    populateCurrentLocation(restaurant)
                    showLayout(singleLocationLayout)
                }
            })
        suggestionsView.layoutManager = LinearLayoutManager(context!!)
        suggestionsView.adapter = adapter
    }
}