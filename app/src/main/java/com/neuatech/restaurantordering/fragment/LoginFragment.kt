package com.neuatech.restaurantordering.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.*
import com.neuatech.restaurantordering.Navigation
import com.neuatech.restaurantordering.R
import com.neuatech.restaurantordering.api.AuthApi
import com.neuatech.restaurantordering.auth.AuthenticationManager
import com.neuatech.restaurantordering.common.ApiProvider
import com.neuatech.restaurantordering.common.Outcome
import com.neuatech.restaurantordering.common.ViewModelFactory
import com.neuatech.restaurantordering.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.progress_bar.*

class LoginFragment : Fragment() {

    companion object {

        const val FACEBOOK_EMAIL_PERMISSION = "email"

        fun create() : LoginFragment {
            return LoginFragment()
        }
    }

    private lateinit var viewModel: LoginViewModel

    private lateinit var callbackManager: CallbackManager

    private lateinit var navigation: Navigation

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        callbackManager = CallbackManager.Factory.create()
        val api = ApiProvider.getApi(AuthApi::class.java) as AuthApi

        viewModel = ViewModelProviders.of(this,
            ViewModelFactory { LoginViewModel(callbackManager, api) })
            .get(LoginViewModel::class.java)

        viewModel.outcome.observe(this, Observer {
            when (it) {
                is Outcome.Success -> {
                    showHideProgress(false)
                    AuthenticationManager.storeToken(context!!, it.value)
                    navigation.onSearchRestaurant()
                }
                is Outcome.Failure -> {
                    showHideProgress(false)
                    //TODO: Show error message
                }
                is Outcome.Loading -> showHideProgress(true)
            }
        })

        facebookLoginButton.setOnClickListener {
            viewModel.login(this)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        navigation = context as Navigation
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun showHideProgress(show: Boolean) {
        progress_bar.visibility = if(show) View.VISIBLE else View.GONE
    }
}