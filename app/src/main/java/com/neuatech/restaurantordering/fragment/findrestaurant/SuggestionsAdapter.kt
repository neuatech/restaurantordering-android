package com.neuatech.restaurantordering.fragment.findrestaurant

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.neuatech.restaurantordering.R
import com.neuatech.restaurantordering.api.model.RestaurantModel

class SuggestionsAdapter(
    private val context: Context,
    private val restaurants: ArrayList<RestaurantModel>,
    private val listener: OnSuggestionClicked
) : RecyclerView.Adapter<SuggestionsAdapter.SuggestionsViewHolder>() {

    inner class SuggestionsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTv: TextView = itemView.findViewById(R.id.restaurantNameTextView)
        val addressTv: TextView = itemView.findViewById(R.id.addressTextView)
        val distanceTv: TextView = itemView.findViewById(R.id.distanceTextView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SuggestionsViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.restaurant_suggestion_layout, parent, false)
        return SuggestionsViewHolder(view)
    }

    override fun getItemCount(): Int = restaurants.size

    override fun onBindViewHolder(holder: SuggestionsViewHolder, position: Int) {
        val restaurant = restaurants[position]
        holder.nameTv.text = restaurant.name
        holder.addressTv.text = restaurant.address
        holder.distanceTv.text = "${restaurant.distance}km"

        holder.itemView.setOnClickListener { listener.onClick(restaurant) }
    }

    interface OnSuggestionClicked {
        fun onClick(restaurant: RestaurantModel)
    }
}