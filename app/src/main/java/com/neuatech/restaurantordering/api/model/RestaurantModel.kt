package com.neuatech.restaurantordering.api.model

data class RestaurantModel(
    val name: String,
    val address: String,
    val distance: Int
)