package com.neuatech.restaurantordering.api.model

import com.google.gson.annotations.SerializedName

data class FacebookUser (
    @SerializedName(value = "accessToken")
    val accessToken: String,

    @SerializedName(value = "firstName")
    val firstName: String,

    @SerializedName(value = "lastName")
    val lastName: String,

    @SerializedName(value = "email")
    val email: String
)