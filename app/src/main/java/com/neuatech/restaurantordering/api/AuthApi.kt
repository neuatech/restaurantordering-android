package com.neuatech.restaurantordering.api

import com.neuatech.restaurantordering.api.model.FacebookUser
import com.neuatech.restaurantordering.api.model.ResponseModel
import kotlinx.coroutines.Deferred
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface AuthApi {

    @Headers(
        "Content-Type: application/json"
    )
    @POST("authenticate")
    fun getJWTTokenAsync(@Body user: FacebookUser) : Deferred<ResponseModel>
}