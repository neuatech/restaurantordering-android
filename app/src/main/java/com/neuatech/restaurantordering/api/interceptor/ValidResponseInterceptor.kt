package com.neuatech.restaurantordering.api.interceptor

import com.google.gson.Gson
import com.neuatech.restaurantordering.api.model.ResponseModel
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import java.io.IOException

class ValidResponseInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val oldResponse = chain.proceed(chain.request())
        val body = oldResponse.body!!
        val bodyAsString = body.string()

        val response: ResponseModel? = try {
            Gson().fromJson(bodyAsString, ResponseModel::class.java)
        } catch (e: Exception) {
            throw IOException("Error while deserializing the response.")
        }

        if (response!!.status < 200 || response.status > 300) {
            throw IOException(response.payload as String)
        }

        return Response.Builder()
            .cacheResponse(oldResponse.cacheResponse)
            .handshake(oldResponse.handshake)
            .networkResponse(oldResponse.networkResponse)
            .priorResponse(oldResponse.priorResponse)
            .protocol(oldResponse.protocol)
            .headers(oldResponse.headers)
            .body(ResponseBody.create(body.contentType(), bodyAsString))
            .code(oldResponse.code)
            .request(oldResponse.request)
            .message(oldResponse.message)
            .build()
    }
}