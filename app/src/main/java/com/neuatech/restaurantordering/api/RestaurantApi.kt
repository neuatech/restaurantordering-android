package com.neuatech.restaurantordering.api

import com.neuatech.restaurantordering.api.model.ResponseModel
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface RestaurantApi {

    @GET("restaurants")
    fun getNearbyRestaurantsAsync(
        @Header("Authorization") token: String,
        @Query("latitude") latitude: Double,
        @Query("longitude") longitude: Double
    ) : Deferred<ResponseModel>
}