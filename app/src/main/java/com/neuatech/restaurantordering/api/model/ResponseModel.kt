package com.neuatech.restaurantordering.api.model

import com.google.gson.annotations.SerializedName

data class ResponseModel(
    @SerializedName(value = "status")
    val status: Int,

    @SerializedName(value = "payload")
    val payload: Any
)