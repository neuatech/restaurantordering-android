package com.neuatech.restaurantordering.common

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.neuatech.restaurantordering.api.AuthApi
import com.neuatech.restaurantordering.api.RestaurantApi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiProvider {
    private val cache = mutableMapOf<String, Any>()

    fun <T> getApi(clazz: Class<T>): Any {
        return when(clazz) {
            AuthApi::class.java -> get(
                OkHttpClientProvider.basicHttpClient,
                clazz,
                ConfigConstants.AUTH_API_URL
            )
            RestaurantApi::class.java -> get(
                OkHttpClientProvider.basicHttpClient,
                clazz,
                ConfigConstants.BASE_URL
            )
            else -> get(
                OkHttpClientProvider.basicHttpClient,
                clazz,
                ConfigConstants.BASE_URL
            )
        }
    }

    private fun <T> get(httpClient: OkHttpClient, clazz: Class<T>, baseUrl: String): Any {
        if (cache.containsKey(clazz.name)) {
            return cache[clazz.name]!!
        }
        cache[clazz.name] = instantiateApiStub(
            httpClient,
            clazz,
            baseUrl
        ) as Any
        return cache[clazz.name]!!
    }

    private fun <T> instantiateApiStub(
        httpClient: OkHttpClient,
        clazz: Class<T>,
        baseUrl: String
    ): T {
        return Retrofit.Builder()
            .client(httpClient)
            .baseUrl(baseUrl)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(clazz)
    }
}