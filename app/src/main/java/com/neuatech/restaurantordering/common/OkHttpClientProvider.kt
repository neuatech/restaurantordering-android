package com.neuatech.restaurantordering.common

import com.neuatech.restaurantordering.api.interceptor.ValidResponseInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

object OkHttpClientProvider {

    val basicHttpClient : OkHttpClient by lazy {
        OkHttpClient.Builder()
            .connectTimeout(5, TimeUnit.SECONDS) // connect timeout
            .writeTimeout(5, TimeUnit.SECONDS) // write timeout
            .readTimeout(5, TimeUnit.SECONDS)
            .addInterceptor(ValidResponseInterceptor())
            .addNetworkInterceptor(HttpLoggingInterceptor().apply {
                level =  HttpLoggingInterceptor.Level.BODY
            })
            .build()
    }
}