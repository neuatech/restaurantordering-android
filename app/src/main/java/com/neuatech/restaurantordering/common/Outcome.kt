package com.neuatech.restaurantordering.common

sealed class Outcome<out T : Any> {

    object Loading : Outcome<Nothing>()

    class Success<out T: Any>(val value: T) : Outcome<T>()

    class Failure(val message: String) : Outcome<String>()
}