package com.neuatech.restaurantordering.common

object ConfigConstants {
    const val BASE_URL = "http://localhost:8050/api/"

    const val AUTH_API_URL = "${BASE_URL}users/"
}