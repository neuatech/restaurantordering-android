package com.neuatech.restaurantordering.location

import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.os.Looper
import androidx.core.location.LocationManagerCompat
import com.google.android.gms.location.*
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class LocationManager(context: Context) {

    private var locationManager: LocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    private var locationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)

    private lateinit var locationCallback : LocationCallback

    fun isLocationEnabled() : Boolean {
        return LocationManagerCompat.isLocationEnabled(locationManager)
    }

    suspend fun getLocation() =
        suspendCoroutine<Location> {continuation ->
            locationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    locationResult ?: return

                    continuation.resume(locationResult.lastLocation)
                    locationClient.removeLocationUpdates(this)
                }
            }

            locationClient.requestLocationUpdates(
                LocationRequest().apply {
                    interval = 5000
                    smallestDisplacement = 20.toFloat()
                },
                locationCallback,
                Looper.getMainLooper()
            )
        }
}