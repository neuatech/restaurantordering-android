package com.neuatech.restaurantordering

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.neuatech.restaurantordering.auth.AuthenticationManager
import com.neuatech.restaurantordering.fragment.LoginFragment
import com.neuatech.restaurantordering.fragment.findrestaurant.FindRestaurantFragment
import com.neuatech.restaurantordering.utils.pushFragment
import com.neuatech.restaurantordering.utils.startUpToolbar

class SingleActivity : AppCompatActivity(), Navigation {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single)
        startUpToolbar()

        if (!AuthenticationManager.isLoggedIn(applicationContext!!)) {
            onLogin()
            return
        }

        onSearchRestaurant()
    }

    override fun onLogin() {
        pushFragment(LoginFragment.create())
    }

    override fun onSearchRestaurant() {
        pushFragment(FindRestaurantFragment.create())
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            //Finish app
            finish()
        }
        super.onBackPressed()
    }
}
